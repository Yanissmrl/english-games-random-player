## English Games

"English Games Random Player" is a small project undertaken during the English Games at My Digital School. It was a week dedicated to communicating exclusively in English, during which we were tasked with conceptualizing a game—a fun board game that offers the opportunity to learn about the digital field. In our case, the focus was on the realm of graphic design.
