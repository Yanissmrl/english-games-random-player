let counter = 0;
let timer;
let seconds = 0;

function AddInput() {
  if (counter <= 4) {
    let input = document.createElement("input");
    input.type = "text";
    input.name = "input";
    input.placeholder = "Name";
    input.className = "input";
    input.id = "formId";
    document.getElementById("inputContainer").appendChild(input);
    let hr = document.createElement("hr");
    hr.className = "hr";
    document.getElementById("inputContainer").appendChild(hr);
  } else {
    let alert = document.createElement("p");
    alert.className = "alert";
    alert.innerHTML = "You have reached the maximum number of fields";
    counter <= 6 && document.getElementById("inputContainer").appendChild(alert);
  }
  counter++;
}

function recupererDonnees() {
  let inputs = document.querySelectorAll('[class^="input"]');
  let inputValues = [];

  inputs.forEach(function (input) {
    input.value && inputValues.push(input.value);
  });

  if (inputValues.length > 0) {
    console.log("Contenu des inputs :", inputValues);
    document.getElementById("formSection").style.display = "none";
    document.getElementById("resultSection").style.display = "flex";
    const random = Math.floor(Math.random() * inputValues.length);
    console.log(random, inputValues[random]);
    let winner = document.createElement("p");
    winner.innerHTML = inputValues[random];
    document.getElementById("winerSpan").innerHTML = winner.innerHTML;
  } else if (counter <= 1) {
    let alert = document.createElement("p");
    alert.className = "alert";
    alert.id = "alert";
    alert.innerHTML = "you cannot launch without providing a name";
    document.getElementById("inputContainer").appendChild(alert);

    setTimeout(() => {
      console.log("bla");
      document.getElementById("alert").remove();
    }, 200);
  }

  counter++;
}

function reset() {
  location.reload();
}

function startChrono() {
  console.log("start btn chrono");
  document.getElementById('btnStart').style.display = 'none';
  document.getElementById('btnStop').style.display = 'flex';
  document.getElementById('btnReset').style.display = 'flex';

  timer = setInterval(updateChrono, 1000);
}

function stopChrono() {
  clearInterval(timer);
  document.getElementById('btnStart').style.display = 'flex';
  document.getElementById('btnStop').style.display = 'none';
  document.getElementById('btnReset').style.display = 'none';
}

function resetChrono() {
  stopChrono();
  test = true;
  updateChrono(test)
  document.getElementById('btnStart').style.display = 'flex';
  document.getElementById('btnStop').style.display = 'none';
}
let test = false;

function updateChrono(test) {
  seconds++;
  if (test) {
    seconds = 0;
  }
  const minutes = Math.floor(seconds / 60);
  const remainingSeconds = seconds % 60;
  document.getElementById('chrono').innerText = `${remainingSeconds < 10 ? '0' : ''}${remainingSeconds}`;


  if (seconds >= 45) {
    stopChrono();
    console.log("stop chrono");
    // alert('Le temps est écoulé!');
  }
  test = false;

}

AddInput();